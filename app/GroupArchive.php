<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupArchive extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'group_archive';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','user_role', 'user_position', 'group_id', 'payment_id', 'status'];

    /**
     * Relation to users table
     *
     * @var array
     */

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    /**
     * Relation to group table
     *
     * @var array
     */

    public function group()
    {
        return $this->belongsTo('App\NvestmentGroup', 'group_id');
    }

    /**
     * Relation to payment details table
     *
     * @var array
     */

    public function payment()
    {
        return $this->belongsTo('App\PaymentDetails', 'payment_id');
    }
}
