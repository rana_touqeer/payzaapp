<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Auth;

class AuthController extends Controller
{


	public function login() {
        if (Auth::check()) {
            return redirect()->intended('admin/dashboard');
        }else{
            return view('admin.auth.login');
        }

	}
    
    public function post(Request $request) {

        $rules = [
            'password' => ['required', 'max:50', 'min:6'],
            'email' => ['required', 'email']
        ];

        $payload = $request->only('password', 'email');

        $validator = app('validator')->make($payload, $rules);

        if ($validator->fails()) {
    		return back()->withErrors($validator)
                        ->withInput();
        }



        if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'role' => '1'])) {
            
            return redirect()->intended('admin/dashboard');
        } else {
    		return back()->withErrors(['msg' => 'Invalid email or password, or you do not have admin rights!'])
                        ->withInput();
        }


       

    }


    public function logout() {
    	Auth::logout();
    	return redirect('admin');
    }

}
