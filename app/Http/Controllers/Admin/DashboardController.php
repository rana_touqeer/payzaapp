<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Api\NvstGroupController;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App\NvestmentGroup;
use App\GroupArchive;
use Carbon\Carbon;
use Helperclass;
use Yajra\Datatables\Datatables;

class DashboardController extends Controller
{

    public function listing() {
        return view('admin.groups.list');
    }
    public function completed_listing(){
        $completed_groups = NvestmentGroup::with('master')->where('status', 'Completed')->get();
        return Datatables::of($completed_groups)->add_column('name', function($group) {
            return $group->master->user->name;
        })->add_column('amount', function($group) {
            return $group->master->payment->amount;
        })->add_column('email', function($group) {
            return $group->master->user->email;
        })->add_column('status', function() {
            return 'Completed';
        })->add_column('created_at', function($group) {
            return Carbon::parse($group->created_at)->format('M d, Y')." <br /> ".Carbon::createFromTimeStamp(strtotime($group->created_at))->diffForHumans();
        })->make(true);
    }

    public function profile($id) {

        $user = User::with('bank_info')->find($id);
        return view('admin.profile.profile',['user' => $user]);
    }
}
