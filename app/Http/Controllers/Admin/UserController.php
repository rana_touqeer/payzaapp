<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\OauthSessions;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;

class UserController extends Controller
{
    public function listing() {
        return view('admin.user.list');
    }

    public function listing_load(){
        $users = User::with('profile')->with('followers');
        return Datatables::of($users)->add_column('followers', function($user) {
            return $user->followers->count();
        })->add_column('following', function($user) {
            return $user->following->count();
        })->add_column('posts', function($user) {
            return $user->posts->count();
        })->add_column('username', function($user) {
            return ($user->profile)?$user->profile->username:'';
        })->add_column('created_at', function($user) {
            return Carbon::parse($user->created_at)->format('M d, Y')." <br /> ".Carbon::createFromTimeStamp(strtotime($user->created_at))->diffForHumans();
        })->add_column('socialLinks', function($user) {
            if($user->socialLinks){
                $content = '';
                foreach($user->socialLinks as $link){
                    $image = '<img src="/images/website.png" class="social_icon" />';
                    if ($link && $link->icon_url) {
                        $image = '<img src="' .$link->icon_url. '" class="social_icon" />';
                    }
                    $content .= '<a href="' . $link->url . '" target="_blank">'.$image.'</a>';
                }

                return $content;
            }
        })->make(true);
    }




    public function block($id) {

    	$user = User::findorfail($id);

    	$user->status = 'Blocked';
        $user->blocked_at = 'Admin';
    	if($user->save()){
            // expire tokens
            $user_sessions = $user->userSessions()->get();
            foreach($user_sessions as $us){
                $session = OauthSessions::findorfail($us->id);
                $session->tokens()->delete();
            }
        }
        return back();
    }

    public function activate($id) {

    	$user = User::findorfail($id);

    	$user->status = 'Active';
        $user->blocked_at = null;
    	$user->save();

        return back();
    }

    public function delete($id) {

    	$user = User::findorfail($id);
    	$user->delete();

    	// to do delete content or cascade

    	return redirect('admin/user');
    }
}
