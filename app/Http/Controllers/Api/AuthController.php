<?php

namespace App\Http\Controllers\Api;

use App\NvestmentPartners;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use Hash;
use App\User;
use App\NvestmentMasters;
use App\NvestmentInvestors;
use Helperclass;
use App\ResetPassword;
use Carbon\Carbon;
use Mail;
use App\NvestmentGroup;
use App\InviteUser;
use App\UserBankInfo;
use App\PushNotifications;
class AuthController extends Controller
{
    /**
     * Register user
     *
     * @return \Illuminate\Http\Response
     */
    public function register( Request $request)
    {
        $rules = [
            'name' => ['required', 'max:255'],
            'password' => ['required', 'max:100'],
            'email' => ['email', 'required','unique:users'],
            'device_token' => ['max:100', 'min:3'],
        ];

        $payload = app('request')->only('name', 'password', 'email', 'device_token');
        $validator = app('validator')->make($payload, $rules);
        if ($validator->fails()) {
            $errors =  Helperclass::formaterrors($validator->errors());
            return response()->json(['status_code' => '210', 'message' => 'errors', 'status_description'   => $errors]);
        }
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->device_token = $request->device_token;
        $user->password = Hash::make($request->password);
        $user->save();

        return $this->login($request);
        /*return response()->json(['status_code' => '200', 'message' => 'success']);*/
    }

    /**
     * Register Investor
     *
     * @return \Illuminate\Http\Response
     */
    public function add_investor( Request $request)
    {
        //Not more than 2 investors per master

        $logged_in_user = JWTAuth::toUser($request->token);
        $master_invester_group = NvestmentMasters::with('group')->where('master_id', $logged_in_user->id)->first();
        $get_existing_investors = NvestmentInvestors::where('master_id', $logged_in_user->id)->where('group_id', $master_invester_group->group_id)->count();
        if($get_existing_investors == 2){
            return response()->json(['status_code' => '211', 'message' => 'errors', 'status_description'   => 'You can not add more than two investors.']);
        }

        //For current master's existing investor check
        $existing_invstor_record = User::where('email', $request->email)->first();
        if($existing_invstor_record){
            $existing_invstor = NvestmentInvestors::where('investor_id',$existing_invstor_record->id)->where('master_id',$logged_in_user->id)->where('group_id',$master_invester_group->group_id)->first();
            if($existing_invstor){
                return response()->json(['status_code' => '212', 'message' => 'errors', 'status_description'   => 'Already added as your investor.']);
            }
            $already_playing = false;
            $another_group_master = NvestmentMasters::where('master_id',$existing_invstor_record->id)->first();
            if($another_group_master){
                $already_playing = true;
            }

            $another_group_investor = NvestmentInvestors::where('investor_id',$existing_invstor_record->id)->first();
            if($another_group_investor){
                $already_playing = true;
            }

            $another_group_partner = NvestmentPartners::where('partners_id',$existing_invstor_record->id)->first();
            if($another_group_partner){
                $already_playing = true;
            }

            if($already_playing){
                return response()->json(['status_code' => '213', 'message' => 'errors', 'status_description'   => 'Investor is already playing in another nvestment group.']);
            }
        }

        //For current master's existing investor check
        $existing_invitation = InviteUser::where('email', $request->email)->where('sender_id', $logged_in_user->id)->where('group_id', $master_invester_group->group_id)->first();
        if($existing_invitation){
                return response()->json(['status_code' => '214', 'message' => 'errors', 'status_description'   => 'You already sent invitation for this nvestment group.']);
        }

        //For new investor

        $rules = [
            'email' => ['email', 'required'],
            'group_id' => ['required'],
        ];

        $payload = app('request')->only('email', 'group_id');
        $validator = app('validator')->make($payload, $rules);
        if ($validator->fails()) {
            $errors =  Helperclass::formaterrors($validator->errors());
            return response()->json(['status_code' => '210', 'message' => 'errors', 'status_description'   => $errors]);
        }

        $investor = new InviteUser;
        $investor->email = $request->email;
        $investor->invitation_type = 'investor';
        $investor->group_id = $request->group_id;
        $investor->sender_id = $logged_in_user->id;
        $investor->position  = $request->position;
        $investor->save();

        $data['sender_name'] = $logged_in_user->name;
        $data['sender_email'] = $logged_in_user->email;
        $email = $request->email;
        $checExisting = User::where('email', $email)->first();
        if($checExisting && $checExisting->device_token && $checExisting->device_token != 'failed'){
            $pn_data = array();
            $pn_data['device_token'] = $checExisting->device_token;
            $pn_data['message'] = $logged_in_user->name . ' just sent you an invitation to play at NvstApp as an investor partner.';
            PushNotifications::sendToIos($pn_data);
        }else{
            $this->setMailer();
            try{
                Mail::send('emails.sendinvestoremail', ['data' => $data], function ($m) use ($email) {
                    $m->from('admin@nsvtapp.com', 'Nvst');
                    $m->to($email)->subject('NvstApp investor request!');
                });
                return response()->json(['status_code' => '200', 'message' => 'Email Sent']);
            }catch (Exception $e){
                $error = $e->getMessage();
                return response()->json(['status_code' => '201', 'message' => 'errors', 'status_description'   => $error]);
            }
        }
        return response()->json(['status_code' => '200', 'message' => 'success']);
    }

    /**
     * Register Partner
     *
     * @return \Illuminate\Http\Response
     */
    public function add_partner( Request $request)
    {
        //Not more than 2 investors per master

        $logged_in_user = JWTAuth::toUser($request->token);
        $investor_group = NvestmentInvestors::with('group')->where('investor_id', $logged_in_user->id)->first();
        $get_existing_partners = NvestmentPartners::where('investor_id', $logged_in_user->id)->where('group_id', $investor_group->group_id)->count();
        if($get_existing_partners == 2){
            return response()->json(['status_code' => '211', 'message' => 'errors', 'status_description'   => 'You can not add more than two partners.']);
        }

        //For current master's existing investor check
        $existing_partner_record = User::where('email', $request->email)->first();
        if($existing_partner_record){
            $existing_partner = NvestmentPartners::where('partners_id',$existing_partner_record->id)->where('investor_id',$logged_in_user->id)->where('group_id',$investor_group->group_id)->first();
            if($existing_partner){
                return response()->json(['status_code' => '212', 'message' => 'errors', 'status_description'   => 'Already added as as partner in this nvestment group.']);
            }

            $already_playing = false;
            $another_group_master = NvestmentMasters::where('master_id',$existing_partner_record->id)->first();
            if($another_group_master){
                $already_playing = true;
            }

            $another_group_investor = NvestmentInvestors::where('investor_id',$existing_partner_record->id)->first();
            if($another_group_investor){
                $already_playing = true;
            }

            $another_group_partner = NvestmentPartners::where('partners_id',$existing_partner_record->id)->first();
            if($another_group_partner){
                $already_playing = true;
            }
            if($already_playing){
                return response()->json(['status_code' => '213', 'message' => 'errors', 'status_description'   => 'Partner is already playing in another nvestment group.']);
            }
        }

        //For current investor's existing partner check
        $existing_invitation = InviteUser::where('email', $request->email)->where('sender_id', $logged_in_user->id)->where('group_id', $investor_group->group_id)->first();
        if($existing_invitation){
            return response()->json(['status_code' => '214', 'message' => 'errors', 'status_description'   => 'You already sent invitation for this nvestment group.']);
        }

        //For new partner

        $rules = [
            'email' => ['email', 'required'],
            'group_id' => ['required'],
        ];

        $payload = app('request')->only('email', 'group_id');
        $validator = app('validator')->make($payload, $rules);
        if ($validator->fails()) {
            $errors =  Helperclass::formaterrors($validator->errors());
            return response()->json(['status_code' => '210', 'message' => 'errors', 'status_description'   => $errors]);
        }

        $investor = new InviteUser;
        $investor->email = $request->email;
        $investor->invitation_type = 'partner';
        $investor->group_id = $request->group_id;
        $investor->sender_id = $logged_in_user->id;
        $investor->position  = $request->position;
        $investor->save();

        $data['sender_name'] = $logged_in_user->name;
        $data['sender_email'] = $logged_in_user->email;
        $email = $request->email;
        $checExisting = User::where('email', $email)->first();
        if($checExisting && $checExisting->device_token && $checExisting->device_token != 'failed'){
            $pn_data = array();
            $pn_data['device_token'] = $checExisting->device_token;
            $pn_data['message'] = $logged_in_user->name . ' just sent you an invitation to play at NvstApp as an investor partner.';
            PushNotifications::sendToIos($pn_data);
        }else {
            $this->setMailer();
            try {
                Mail::send('emails.sendinvestoremail', ['data' => $data], function ($m) use ($email) {
                    $m->from('admin@nsvtapp.com', 'Nvst');
                    $m->to($email)->subject('NvstApp investor request!');
                });
                return response()->json(['status_code' => '200', 'message' => 'Email Sent']);
            } catch (Exception $e) {
                $error = $e->getMessage();
                return response()->json(['status_code' => '201', 'message' => 'errors', 'status_description' => $error]);
            }
        }

        return response()->json(['status_code' => '200', 'message' => 'success']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $input = $request->all();
        $credentials = array('email' => $request->email, 'password' => $request->password);
        $user = User::where('email', $request->email)->first();
        if ($user && Hash::check($request->password, $user->password)){
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['status_code' => '210', 'message' => 'errors', 'status_description'   => 'wrong email or password.']);
            }
        }else{
            return response()->json(['status_code' => '210', 'message' => 'errors', 'status_description'   => 'wrong email or password.']);
        }
        $data['token'] = $token;
        $user = JWTAuth::toUser($token);
        $user_details = array();
        $user_details['user_id'] = $user->id;
        $user_details['name'] = $user->name;
        $user_details['email'] = $user->email;


        if($request->device_token != $user->device_token){
            if($user->saveDeviceToken($user, $request->device_token)){
                $data['device_token_status'] = 'saved';
            }else{
                $data['device_token_status'] = 'not saved';
            }
        }else{
            $data['device_token_status'] = 'already saved';
        }


        /*get invites*/
        $email = $user->email;

        /*Get user type*/
        $user_type = null;
        $master = NvestmentMasters::where('master_id', $user->id)->first();
        $group_id = null;
        if($master){
            $group_id = $master->group_id;
            $user_type = 'master';
        }
        $position = "";
        $invester = NvestmentInvestors::where('investor_id', $user->id)->first();
        if($invester){
            $group_id = $invester->group_id;
            $user_type = 'investor';
            $position = $invester->position;
        }

        $partner = NvestmentPartners::where('partners_id', $user->id)->first();
        if($partner){
            $group_id = $partner->group_id;
            $user_type = 'partner';
            $position = $partner->position;
        }
        $group_progress = array();
        if($group_id){
            $group_progress = GroupActionsController::group_progress($group_id);
        }

        $invite_details = array();
        if(!$user_type){
            $invite = InviteUser::where('email', $email)->first();
            if($invite){
                $sender_details = User::where('id', $invite->sender_id)->first();
                $user_details['user_type'] = '';
                $invite_details['invite_id'] = $invite->id;
                $invite_details['group_id'] = $invite->group_id;
                $invite_details['sender_id'] = $sender_details->id;
                $invite_details['name'] = $sender_details->name;
                $invite_details['email'] = $sender_details->email;
                $invite_details['invitation_type'] = $invite->invitation_type;
                $user_details['user_type'] = $invite->invitation_type;
            }else{
                $user_details['user_type'] = 'master';
            }
        }else{
            $user_details['user_type'] = $user_type;

        }
        $user_details['position'] = $position;
        $bank_info = null;
        $bank_info_record = UserBankInfo::where('user_id', $user->id)->first();
        if($bank_info_record){
            $bank_info = $bank_info_record;
        }
        $data['user_detail'] = $user_details;
        $data['group_details'] = $group_progress;
        $data['invite_details'] = $invite_details;
        $data['bank_info'] = $bank_info;
        return response()->json(['status_code'  => '200', 'message' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login_by_uid(Request $request)
    {
        $user = User::where('id', $request->user_id)->first();
        if ($user){
            if (!$token = JWTAuth::fromUser($user)) {
                return response()->json(['status_code' => '210', 'message' => 'errors', 'status_description'   => 'wrong email or password.']);
            }
        }else{
            return response()->json(['status_code' => '210', 'message' => 'errors', 'status_description'   => 'wrong email or password.']);
        }
        $data['token'] = $token;
        $user = JWTAuth::toUser($token);
        $user_details = array();
        $user_details['user_id'] = $user->id;
        $user_details['name'] = $user->name;
        $user_details['email'] = $user->email;


        if($request->device_token != $user->device_token){
            if($user->saveDeviceToken($user, $request->device_token)){
                $data['device_token_status'] = 'saved';
            }else{
                $data['device_token_status'] = 'not saved';
            }
        }else{
            $data['device_token_status'] = 'already saved';
        }


        /*get invites*/
        $email = $user->email;

        /*Get user type*/
        $user_type = null;
        $master = NvestmentMasters::where('master_id', $user->id)->first();
        $group_id = null;
        if($master){
            $group_id = $master->group_id;
            $user_type = 'master';
        }
        $position = "";
        $invester = NvestmentInvestors::where('investor_id', $user->id)->first();
        if($invester){
            $group_id = $invester->group_id;
            $user_type = 'investor';
            $position = $invester->position;
        }

        $partner = NvestmentPartners::where('partners_id', $user->id)->first();
        if($partner){
            $group_id = $partner->group_id;
            $user_type = 'partner';
            $position = $invester->position;
        }
        $group_progress = array();
        if($group_id){
            $group_progress = GroupActionsController::group_progress($group_id);
        }

        $invite_details = array();
        if(!$user_type){
            $invite = InviteUser::where('email', $email)->first();
            if($invite){
                $sender_details = User::where('id', $invite->sender_id)->first();
                $user_details['user_type'] = '';
                $invite_details['invite_id'] = $invite->id;
                $invite_details['group_id'] = $invite->group_id;
                $invite_details['sender_id'] = $sender_details->id;
                $invite_details['name'] = $sender_details->name;
                $invite_details['email'] = $sender_details->email;
                $invite_details['invitation_type'] = $invite->invitation_type;
                $user_details['user_type'] = $invite->invitation_type;
            }else{
                $user_details['user_type'] = 'master';
            }
        }else{
            $user_details['user_type'] = $user_type;

        }
        $user_details['position'] = $position;
        $bank_info = null;
        $bank_info_record = UserBankInfo::where('user_id', $user->id)->first();
        if($bank_info_record){
            $bank_info = $bank_info_record;
        }
        $data['user_detail'] = $user_details;
        $data['group_details'] = $group_progress;
        $data['invite_details'] = $invite_details;
        $data['bank_info'] = $bank_info;
        return response()->json(['status_code'  => '200', 'message' => $data]);
    }

    public function logout(Request $request){
        $user = JWTAuth::invalidate($request->token);
        return response()->json(['status_code'  => '200', 'message' => 'logged out successfuly']);
    }

    /**
     * Get user detail by log in token
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function get_user_details(Request $request)

    {

        $input = $request->all();

        $user = JWTAuth::toUser($input['token']);
        $data['user_detail'] = $user;
        return response()->json(['status_code'  => 200, 'message' => $data]);
    }

    /**
     * forgot password
     *
     * @return \Illuminate\Http\Response
     */
    public function forgot(Request $request)
    {

        $rules = [
            'email' => ['required', 'email']
        ];

        $payload = app('request')->only('email');

        $validator = app('validator')->make($payload, $rules);

        if ($validator->fails()) {
            $errors =  Helperclass::formaterrors($validator->errors());
            return response()->json(['status_code' => 210, 'message' => 'errors', 'status_description'   => $errors]);
        }

        $user = User::where('email', $request->email)->first();

        if ($user) {

            $obj = new ResetPassword();
            $dt = new Carbon();

            $obj->user_id = $user->id;
            $obj->token = $obj->generateSecret();
            $obj->expiry = $dt->addMinute(15);
            $obj->save();

            $data['name'] = $user->name;
            $data['token'] = $obj->token;
            $this->setMailer();
            try{

                Mail::send('emails.changepassword', ['data' => $data], function ($m) use ($user) {
                    $m->from('admin@nsvtapp.com', 'Nvst');
                    $m->to($user->email)->subject('Change Password Request');
                });
                return response()->json(['status_code' => 200, 'message' => 'Email Sent']);
            }catch (Exception $e){
                $error = $e->getMessage();
                return response()->json(['status_code' => 201, 'message' => 'errors', 'status_description'   => $error]);
            }
        } else {
            return response()->json(['status_code' => 202, 'message' => 'errors', 'status_description'   => 'User not found']);
        }

    }

    private function setMailer()
    {
        $options['ssl']['verify_peer'] = FALSE;
        $options['ssl']['verify_peer_name'] = FALSE;
        $options['ssl']['allow_self_signed'] = TRUE;

        // Send email notification
        $transport = \Swift_SmtpTransport::newInstance(
            \Config::get('mail.host'),
            \Config::get('mail.port'),
            \Config::get('mail.encryption')
        )
            ->setUsername(\Config::get('mail.username'))
            ->setPassword(\Config::get('mail.password'))
            ->setStreamOptions($options);

        $mailer = \Swift_Mailer::newInstance($transport);
        Mail::setSwiftMailer($mailer);
    }
}
