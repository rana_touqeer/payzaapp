<?php

namespace App\Http\Controllers\Api;

use App\NvestmentPartners;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use App\User;
use App\NvestmentMasters;
use App\NvestmentInvestors;
use Helperclass;
use App\NvestmentGroup;
use App\InviteUser;
use App\PushNotifications;
use Carbon\Carbon;
use App\GroupArchive;

class GroupActionsController extends Controller
{
    /**
     * Approve group invitation
     *
     * @return \Illuminate\Http\Response
     */
    public function approve_invitation(Request $request){
        $user = JWTAuth::toUser($request->token);
        $user_id = $user->id;
        $invite_id = $request->invite_id;
        $invitation = InviteUser::find($invite_id);
        $invitation->status = 'Approved';
        $invitation->save();
        $group = NvestmentGroup::where('id', $invitation->group_id)->first();

        $group_array = array(
            'group_id'  => $group->id,
            'nvestment_amount'  => $group->nvestment_amount,
        );
        if($invitation->invitation_type == 'investor'){
            $sender_details = User::find($invitation->sender_id);
            $sender_user = array(
                'user_type' => 'master',
                'user_id'   => $sender_details->id,
                'name'   => $sender_details->name,
                'email'   => $sender_details->email,
            );
        }

        if($invitation->invitation_type == 'partner'){
            $sender_details = User::find($invitation->sender_id);
            $sender_user = array(
                'user_type' => 'invester',
                'user_id'   => $sender_details->id,
                'name'   => $sender_details->name,
                'email'   => $sender_details->email,
            );
        }


        $return_array = array(
            'group_details' => $group_array,
            'sender_details' => $sender_user,
            'invitation_type' => $invitation->invitation_type,
            'status' => $invitation->status,
        );
        //Delete all other invitations
        InviteUser::where('id', '!=' , $invite_id)->where('email', $user->email)->delete();
        return response()->json(['status_code'  => '200', 'message' => $return_array]);
    }

    /**
     * Delete invitations on successful charge.
     *
     * @return \Illuminate\Http\Response
     */
    public static function delete_invitations($email){
        InviteUser::where('email', $email)->delete();
        return true;
    }

    public function group_information(Request $request){
        $user = JWTAuth::toUser($request->token);
        $group_id = $request->group_id;
        $data['group_details'] = $this::group_progress($group_id);
        return response()->json(['status_code'  => '200', 'message' => $data]);
    }

    public function invites_information(Request $request){
        $user = JWTAuth::toUser($request->token);
        $group_id = $request->group_id;
        $data['invite_details'] = $this::get_invite_details($group_id);
        return response()->json(['status_code'  => '200', 'message' => $data]);
    }

    /**
     * Release user from a group
     *
     * @return \Illuminate\Http\Response
     */
    public function release_player(Request $request){
        $group_id = $request->group_id;
        $player_id = $request->user_id;
        $player_type = $request->user_type;
        if($player_type == 'master'){
            $delete_master = NvestmentMasters::where('master_id', $player_id)->where('group_id',$group_id)->delete();
            if($delete_master){
                return response()->json(['status_code' => '200', 'message' => 'Player association removed from completed group.']);
            }else{
                return response()->json(['status_code' => '210', 'message' => 'error', 'status_description' => 'Player association could not be removed.']);
            }
        }else if($player_type == 'investor'){
            $delete_investor = NvestmentInvestors::where('investor_id', $player_id)->where('group_id',$group_id)->delete();
            if($delete_investor){
                return response()->json(['status_code' => '200', 'message' => 'Player association removed from completed group.']);
            }else{
                return response()->json(['status_code' => '210', 'message' => 'error', 'status_description' => 'Player association could not be removed.']);
            }
        }else if($player_type == 'partner'){
            $delete_investor = NvestmentPartners::where('partners_id', $player_id)->where('group_id',$group_id)->delete();
            if($delete_investor){
                return response()->json(['status_code' => '200', 'message' => 'Player association removed from completed group.']);
            }else{
                return response()->json(['status_code' => '210', 'message' => 'error', 'status_description' => 'Player association could not be removed.']);
            }
        }
    }

    /**
     * Get group progress
     *
     * @return group progress array
     */
    public static function group_progress($group_id){
        $group = NvestmentGroup::where('id', $group_id)->first();
        $data = array();
        $group_details = array();
        if($group){
            $cDate = Carbon::parse($group->created_at);
            $group_details['group_id'] = $group->id;
            $group_details['nvestment_amount'] = $group->nvestment_amount;
            $group_details['days_past'] = $cDate->diffInDays();
            $group_details['group_status'] = $group->status;
            $data['group_details'] = $group_details;
        }


        $master_user = array();
        $group_master = NvestmentMasters::where('group_id', $group_id)->first();
        if($group_master){
            $data['percent'] = 0;
            $master_uid = $group_master->master_id;
            $master_payment_id = $group_master->payment_id;
            $master = User::where('id',$master_uid)->first();
            $master_user['user_id'] = $master->id;
            $master_user['name'] = $master->name;
            $master_user['email'] = $master->email;
            $master_user['payment_id'] = $master_payment_id;
            $data['master'] = $master_user;
            $data['percent'] = $data['percent']+16;
            $investors = NvestmentInvestors::where('group_id', $group_id)->get();
            if($investors){
                $i = 0;
                $investor_users = array();
                foreach($investors as $investor){
                    $investor_uid = $investor->investor_id;
                    $investor_payment_id = $investor->payment_id;
                    $investor_position = $investor->position;
                    $investor_user = User::where('id',$investor_uid)->first();

                    $investor_users[$i]['user_id'] = $investor_user->id;
                    $investor_users[$i]['name'] = $investor_user->name;
                    $investor_users[$i]['email'] = $investor_user->email;
                    $investor_users[$i]['payment_id'] = $investor_payment_id;
                    $investor_users[$i]['position'] = $investor_position;
                    $data['percent'] = $data['percent']+14;

                    $partners = NvestmentPartners::where('group_id', $group_id)->where('investor_id', $investor->investor_id)->get();
                    if($partners){
                        $j = 0;
                        $partner_users = array();
                        foreach($partners as $partner){
                            $partner_uid = $partner->partners_id;
                            $partner_payment_id = $partner->payment_id;
                            $partner_position = $partner->position;
                            $partner_user = User::where('id',$partner_uid)->first();

                            $partner_users[$j]['user_id'] = $partner_user->id;
                            $partner_users[$j]['name'] = $partner_user->name;
                            $partner_users[$j]['email'] = $partner_user->email;
                            $partner_users[$j]['payment_id'] = $partner_payment_id;
                            $partner_users[$j]['position'] = $partner_position;
                            $data['percent'] = $data['percent']+14;
                            $j++;
                        }
                        $investor_users[$i]['partners'] = $partner_users;
                    }
                    $i++;
                }
                $data['investors'] = $investor_users;
                if($data['percent'] == 100){
                    //Process after completion actions
                    self::group_complete_actions($data);
                    $group = NvestmentGroup::find($group->id);
                    $group->status  = 'Completed';
                    $group->save();
                }
            }
        }
        return $data;
    }

    /**
     * Send group completion push notifications notifications
     *
     * @return true
     */
    public static function group_completion_notification($group_id){
        $group_master = NvestmentMasters::where('group_id',$group_id)->first();
        if($group_master){
            $master = User::find($group_master->master_id);
            $pn_data = array();
            $pn_data['device_token'] = $master->device_token;
            $pn_data['message'] = 'Congratulations your group# '.$group_id.' is successfully completed.';
            PushNotifications::sendToIos($pn_data);
        }

        $group_investors = NvestmentInvestors::where('group_id', $group_id)->get();
        if($group_investors){
            foreach($group_investors as $group_investor){
                $pn_data = array();
                $investor = User::find($group_investor->investor_id);
                $pn_data['device_token'] = $investor->device_token;
                $pn_data['message'] = 'Congratulations your group# '.$group_id.' is successfully completed.';
                PushNotifications::sendToIos($pn_data);
            }
        }

        $group_partners = NvestmentPartners::where('group_id', $group_id)->get();
        if($group_partners){
            foreach($group_partners as $group_partner){
                $pn_data = array();
                $partner = User::find($group_partner->partners_id);
                $pn_data['device_token'] = $partner->device_token;
                $pn_data['message'] = 'Congratulations your group# '.$group_id.' is successfully completed.';
                PushNotifications::sendToIos($pn_data);
            }
        }

        return true;
    }

    public static function get_invite_details($group_id){
        $data = array();
        $investors = InviteUser::where('group_id', $group_id)->where('invitation_type', 'investor')->get();
        $investor_array = array();
        $i = 0;
        foreach($investors as $investor){
            $investor_array[$i]['master_id'] = $investor->sender_id;
            $investor_array[$i]['email'] = $investor->email;
            $investor_array[$i]['position'] = $investor->position;
            $i++;
        }

        $partners = InviteUser::where('group_id', $group_id)->where('invitation_type', 'partner')->get();
        $partner_array = array();
        $j = 0;
        foreach($partners as $partner){
            $partner_array[$j]['investor_id'] = $partner->sender_id;
            $partner_array[$j]['email'] = $partner->email;
            $partner_array[$j]['position'] = $partner->position;
            $j++;
        }

        $data['investor_invites'] = $investor_array;
        $data['partner_invites'] = $partner_array;
        return $data;
    }

    public static function group_complete_actions($data){
        //archive group information with users
        $group_id = $data['group_details']['group_id'];

        $group_archive = new GroupArchive;
        $group_archive->group_id = $group_id;
        $group_archive->user_id = $data['master']['user_id'];
        $group_archive->user_role = 'master';
        $group_archive->user_position = '1';
        $group_archive->payment_id = $data['master']['payment_id'];
        $group_archive->save();
        if($data['investors']){
            foreach($data['investors'] as $investor){
                $group_archive = new GroupArchive;
                $group_archive->group_id = $group_id;
                $group_archive->user_id = $investor['user_id'];
                $group_archive->user_role = 'investor';
                $group_archive->user_position = $investor['position'];
                $group_archive->payment_id = $investor['payment_id'];
                $group_archive->save();
                //create group for investor
                $create_group = new NvestmentGroup;
                $create_group->nvestment_amount = $data['group_details']['nvestment_amount'];
                $create_group->status = 'Pending';
                $create_group->save();
                $new_group_id = $create_group->id;

                //add investor as master for new group
                $add_master = New NvestmentMasters;
                $add_master->master_id = $investor['user_id'];
                $add_master->group_id = $new_group_id;
                $add_master->payment_id = $investor['payment_id'];
                $add_master->save();
                $k = 1;
                foreach($investor['partners'] as $partner){
                    $group_archive = new GroupArchive;
                    $group_archive->group_id = $group_id;
                    $group_archive->user_id = $partner['user_id'];
                    $group_archive->user_role = 'partner';
                    $group_archive->user_position = $partner['position'];
                    $group_archive->payment_id = $partner['payment_id'];
                    $group_archive->save();

                    //add partner as master for new group
                    $add_investor = New NvestmentInvestors;
                    $add_investor->master_id = $investor['user_id'];
                    $add_investor->investor_id = $partner['user_id'];
                    $add_investor->group_id = $new_group_id;
                    $add_investor->payment_id = $partner['payment_id'];
                    $add_investor->position = $k;
                    $add_investor->save();
                    $k++;
                }
            }
            return true;
        }
        return true;
    }

    public function testpn(){
        $data = array();
        $data['message'] = 'testing test message pn';
        $data['deviceToken'] = "4e51a9177159f8f7e3b57aaff0f245a64978c644d74bfa8adc4ec09f1a8c11f3";
        PushNotifications::sendToIos($data);
    }
}