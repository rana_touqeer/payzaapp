<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use JWTAuth;
use App\NvestmentGroup;

class NvstGroupController extends Controller
{
    /**
     * Api for creating new group
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $rules = [
            'amount' => ['required'],
        ];
        $payload = app('request')->only('amount');
        $validator = app('validator')->make($payload, $rules);
        if ($validator->fails()) {
            $errors =  Helperclass::formaterrors($validator->errors());
            return response()->json(['status_code' => 210, 'status_description' => $errors, 'message'   => 'error']);
        }
        $user = JWTAuth::toUser($request->token);
        $master_id = $user->id;

        $getMasterNvstorGroups = NvestmentGroup::where('master_id', $master_id)->where('status', 'Pending')->first();
        if($getMasterNvstorGroups){
            return response()->json(['status_code' => 210, 'message' => 'error', 'status_description' => 'You are already active on a pending nvstment group!']);
        }

        $amount = $request->amount;
        $group = new NvestmentGroup;
        $group->master_id = $master_id;
        $group->nvestment_amount = $amount;
        $group->status = 'Pending';
        $group->save();
        $data = array();
        $data['user'] = $user;
        $data['nvst_group'] = $group;
        return response()->json(['status_code' => 200, 'message' => $data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
