<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use Hash;
use App\User;
use App\NvestmentMasters;
use App\NvestmentInvestors;
use Helperclass;
use App\ResetPassword;
use Carbon\Carbon;
use Mail;
use App\NvestmentGroup;
use App\InviteUser;
use App\UserBankInfo;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function get(Request $request)
    {
        $user = JWTAuth::toUser($request->token);
        $user_data = array();
        $user_data['id'] = $user->id;
        $user_data['name'] = $user->name;
        $user_data['email'] = $user->email;
        $data['profile'] = $user_data;
        return response()->json(['status_code'  => '200', 'message' => $data]);
    }

    public function save(Request $request){
        $rules = [
            'name' => ['max:255', 'min:3'],
            'email' => ['email','unique:users'],
            'password' => ['min:3'],
        ];
        $payload = app('request')->only('name', 'password', 'email');
        $validator = app('validator')->make($payload, $rules);
        if ($validator->fails()) {
            $errors =  Helperclass::formaterrors($validator->errors());
            return response()->json(['status_code' => '210', 'message' => 'errors', 'status_description'   => $errors]);
        }
        $user = JWTAuth::toUser($request->token);
        if($request->name) {
            $user->name = $request->name;
        }

        if($request->email) {
            $user->email = $request->email;
        }
        if($request->password){
            $user->password = Hash::make($request->password);
        }
        if($user->save()){
            $user_data = array();
            $user_data['id'] = $user->id;
            $user_data['name'] = $user->name;
            $user_data['email'] = $user->email;
            $data['profile'] = $user_data;
            return response()->json(['status_code' => '200', 'message' => $data]);
        }else{
            return response()->json(['status_code' => '210', 'message' => 'errors', 'status_description'   => 'some error occured please try again.']);
        }
    }

    //Save bank information
    public function save_bank_info(Request $request){
        $rules = [
            'bank_holder_name' => ['required', 'max:255', 'min:3'],
            'swift_bic_code' => ['max:255', 'min:3'],
            'account_number' => ['required','max:255', 'min:3'],
            'bank_name' => ['required','max:255', 'min:3'],
            'bank_address' => ['required','max:255', 'min:3'],
        ];
        $payload = app('request')->only('bank_holder_name', 'swift_bic_code', 'account_number', 'bank_name', 'bank_address');
        $validator = app('validator')->make($payload, $rules);
        if ($validator->fails()) {
            $errors =  Helperclass::formaterrors($validator->errors());
            return response()->json(['status_code' => '210', 'message' => 'errors', 'status_description'   => $errors]);
        }
        $user = JWTAuth::toUser($request->token);
        $user_id = $user->id;
        $bank_account = UserBankInfo::where('user_id', $user_id)->first();
        if(!$bank_account){
            $bank_account = New UserBankInfo;
            $bank_account->user_id = $user_id;
        }
        $bank_account->bank_holder_name = $request->bank_holder_name;
        $bank_account->swift_bic_code = $request->swift_bic_code;
        $bank_account->account_number = $request->account_number;
        $bank_account->bank_name = $request->bank_name;
        $bank_account->bank_address = $request->bank_address;
        if($bank_account->save()){
            $data['bank_info'] = $bank_account;
            return response()->json(['status_code' => '200', 'message' => $data]);
        }else{
            return response()->json(['status_code' => '210', 'message' => 'errors', 'status_description'   => 'some error occured please try again.']);
        }
    }
}
