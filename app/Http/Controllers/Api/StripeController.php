<?php

namespace App\Http\Controllers\Api;

use App\InviteUser;
use App\NvestmentGroup;
use App\NvestmentInvestors;
use App\NvestmentMasters;
use App\NvestmentPartners;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Cartalyst\Stripe\Stripe;
use JWTAuth;
use App\User;
use Helperclass;
use App\PaymentDetails;
use App\Http\Controllers\Api\GroupActionsController;
use App\PushNotifications;


class StripeController extends Controller
{
    /**
     * Charge API function
     *
     * @return \Illuminate\Http\Response
     */

    public function charge( Request $request){
        $rules = [
            'token' => ['required'],
            'amount' => ['required'],
            'receipt_email' => ['email', 'required'],
            'currency' => ['required', 'max:10', 'min:2'],
            'stripe_token' => ['required','max:255', 'min:3'],
            'user_type' => ['required'],
        ];
        $payload = app('request')->only('amount', 'receipt_email', 'currency', 'stripe_token', 'user_type', 'token');
        $validator = app('validator')->make($payload, $rules);
        if ($validator->fails()) {
            $errors =  Helperclass::formaterrors($validator->errors());
            return response()->json(['status_code' => '210', 'status_description' => $errors, 'message'   => 'error']);
        }
        try {
            $user = JWTAuth::toUser($request->token);

            if($request->user_type == 'investor'){
                $invite_record =InviteUser::where('email', $user->email)->where('invitation_type', 'investor')->where('group_id', $request->group_id)->first();
                $get_existing_investors = NvestmentInvestors::where('master_id', $invite_record->sender_id)->where('group_id', $invite_record->group_id)->count();
                if($get_existing_investors >= 2){
                    InviteUser::where('id', $invite_record->id)->delete();
                    return response()->json(['status_code' => '211', 'message' => 'errors', 'status_description'   => 'Sorry the investor limit is reached for this investment group.']);
                }
            }

            if($request->user_type == 'partner'){
                $invite_record =InviteUser::where('email', $user->email)->where('invitation_type', 'partner')->where('group_id', $request->group_id)->first();
                $get_existing_partners = NvestmentPartners::where('investor_id', $invite_record->sender_id)->where('group_id', $invite_record->group_id)->count();
                if($get_existing_partners >= 2){
                    InviteUser::where('id', $invite_record->id)->delete();
                    return response()->json(['status_code' => '211', 'message' => 'errors', 'status_description'   => 'Sorry the partner limit is reached for this investor in this investment group.']);
                }
            }


            $stripe = new Stripe('sk_test_uaw1L5sMWqgF2RRu5i2IyXh3', '2017-04-06');
            $charge = $stripe->charges()->create([
                'currency' => $request->currency,
                'amount'   => (int)$request->amount,
                'receipt_email' => $request->receipt_email,
                'source'  => $request->stripe_token,
            ]);

            if($charge['paid'] == 'true'){
                /*Save payment log*/
                $payment_log = new PaymentDetails;
                $payment_log->user_id = $user->id;
                $payment_log->transaction_id = $charge['id'];
                $payment_log->payment_status= 'Paid';
                $payment_log->amount = $charge['amount']/100;
                $payment_log->save();
                if($request->group_id){
                    $group_id = $request->group_id;
                    $group_log = NvestmentGroup::where('id',$group_id)->first();
                }else{
                    /*Create Group Log*/
                    $group_log = new NvestmentGroup;
                    $group_log->nvestment_amount = $charge['amount']/100;
                    $group_log->save();
                    $group_id = $group_log->id;
                }
                $group_status = $group_log->status;
                if($request->user_type == 'master'){
                    $add_player = new NvestmentMasters;
                    $add_player->master_id = $user->id;
                    $add_player->group_id = $group_id;
                    $add_player->payment_id = $payment_log->id;
                    $add_player->save();
                }

                $position = "";

                if($request->user_type == 'investor'){
                    $invite_record =InviteUser::where('email', $user->email)->where('invitation_type', 'investor')->where('group_id', $group_id)->first();
                    $master_id = $invite_record->sender_id;
                    $position = $invite_record->position;
                    $add_player = new NvestmentInvestors;
                    $master_user = User::find($master_id);
                    $add_player->master_id = $master_id;
                    $add_player->investor_id = $user->id;
                    $add_player->group_id = $group_id;
                    $add_player->payment_id = $payment_log->id;
                    $add_player->position = $position;
                    $add_player->save();
                    $position = $add_player->position;
                    GroupActionsController::delete_invitations($user->email);

                    $pn_data['device_token'] = $master_user->device_token;
                    $pn_data['message'] = $user->name.' have successfully enrolled to play in your $'.($charge['amount']/100).' amount group# '.$group_id.' as a group investor.';
                    PushNotifications::sendToIos($pn_data);
                }

                if($request->user_type == 'partner'){
                    $invite_record =InviteUser::where('email', $user->email)->where('invitation_type', 'partner')->where('group_id', $group_id)->first();
                    $investor_id = $invite_record->sender_id;
                    $position = $invite_record->position;
                    $add_player = new NvestmentPartners;
                    $add_player->investor_id = $investor_id;
                    $add_player->partners_id = $user->id;
                    $add_player->group_id = $group_id;
                    $add_player->payment_id = $payment_log->id;
                    $add_player->position = $position;
                    $add_player->save();
                    $position = $add_player->position;
                    GroupActionsController::delete_invitations($user->email);
                    $investor_user = User::find($investor_id);

                    $pn_data['device_token'] = $investor_user->device_token;
                    $pn_data['message'] = $user->name.' have successfully enrolled to play in $'.($charge['amount']/100).' amount group# '.$group_id.' as a group partner.';
                    PushNotifications::sendToIos($pn_data);

                    $master_record = NvestmentMasters::where('group_id',$group_id)->first();
                    $master_user = User::find($master_record->master_id);
                    $pn_data['device_token'] = $master_user->device_token;
                    $pn_data['message'] = $user->name.' have successfully enrolled to play in your $'.($charge['amount']/100).' amount group# '.$group_id.' as a group partner.';
                    PushNotifications::sendToIos($pn_data);
                }

                $group_progress = GroupActionsController::group_progress($group_id);
                if($group_progress['percent'] == 100){
                    $group_log->status  = 'Completed';
                    $group_log->save();
                    GroupActionsController::group_completion_notification($group_id);
                }
                $group_info = NvestmentGroup::find($group_id);
                $group_detail = array();
                $group_detail['group_id'] = $group_id;
                $group_detail['nvestment_amount'] = $group_info->nvestment_amount;
                $group_detail['group_status'] = $group_info->status;
                $group_detail['position'] = $position;
                return response()->json(['status_code' => '200', 'message' => $group_detail]);
            }

        }catch (\Cartalyst\Stripe\Exception\NotFoundException $e) {
            return response()->json(['status_code' => '210', 'message' => 'error', 'status_description' => $e->getMessage()]);
        }
        catch (\Cartalyst\Stripe\Exception\BadRequestException $e) {
            return response()->json(['status_code' => '210', 'message' => 'error', 'status_description' => $e->getMessage()]);
        }
        catch (\Cartalyst\Stripe\Exception\InvalidRequestException $e) {
            return response()->json(['status_code' => '210', 'message' => 'error', 'status_description' => $e->getMessage()]);
        }
        catch (\Cartalyst\Stripe\Exception\CardErrorException $e) {
            return response()->json(['status_code' => '210', 'message' => 'error', 'status_description' => $e->getMessage()]);
        }

        catch (\Cartalyst\Stripe\Exception\NotFoundException $e) {
            return response()->json(['status_code' => '210', 'message' => 'error', 'status_description' => $e->getMessage()]);
        }

        catch (\Cartalyst\Stripe\Exception\MissingParameterException $e) {
            return response()->json(['status_code' => '210', 'message' => 'error', 'status_description' => $e->getMessage()]);
        }
    }
}
