<?php
namespace App\Http\Middleware;

use Closure;

use JWTAuth;

use Exception;

class authJWT

{

    public function handle($request, Closure $next)

    {

        try {

            $user = JWTAuth::toUser($request->input('token'));

        } catch (Exception $e) {

            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {

                return response()->json(['error' => 'Token is Invalid']);

            } else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) {

                return response()->json(['status_code' => '220', 'message' => "error", 'status_description'   => 'Token can not be refreshed please sign in again.']);

            } else {
                return response()->json(['error' => 'Something is wrong']);
            }

        }

        return $next($request);

    }

}