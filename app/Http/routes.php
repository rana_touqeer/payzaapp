<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'PageController@index');

/* Admin End routes...*/

Route::get('admin', 'Admin\AuthController@login');
Route::get('admin/auth/logout', 'Admin\AuthController@logout');
Route::post('admin/auth/post', 'Admin\AuthController@post');

Route::group(['middleware' => 'auth.admin'], function(){
    // Admin Dashboard
    Route::get('admin/dashboard', ['uses' => 'Admin\DashboardController@listing']);
    Route::get('admin/groups/completed', ['as' => 'manage_completed_groups' ,'uses' => 'Admin\DashboardController@completed_listing']);
    Route::get('admin/user/profile/{id}', ['uses' => 'Admin\DashboardController@profile']);
});

$api = app('Dingo\Api\Routing\Router');
$api->version('v1', function ($api) {

    $api->post('register', 'App\Http\Controllers\Api\AuthController@register');
    $api->post('login', 'App\Http\Controllers\Api\AuthController@login');
    $api->post('login_uid', 'App\Http\Controllers\Api\AuthController@login_by_uid');
    $api->post('forgot', ['uses' => 'App\Http\Controllers\Api\AuthController@forgot']);
    $api->get('test_pn', ['uses' => 'App\Http\Controllers\Api\GroupActionsController@testpn']);

    $api->group(['middleware' => 'jwt-auth'], function ($api) {
        $api->post('my_detail', 'App\Http\Controllers\Api\AuthController@get_user_details');
        $api->post('create_group', 'App\Http\Controllers\Api\NvstGroupController@create');
        $api->post('add_investor', 'App\Http\Controllers\Api\AuthController@add_investor');
        $api->post('add_partner', 'App\Http\Controllers\Api\AuthController@add_partner');
        $api->post('release_player', 'App\Http\Controllers\Api\GroupActionsController@release_player');
        $api->post('stripe_charge', 'App\Http\Controllers\Api\StripeController@charge');
        $api->post('approve_invitation', 'App\Http\Controllers\Api\GroupActionsController@approve_invitation');
        $api->post('group_information', 'App\Http\Controllers\Api\GroupActionsController@group_information');
        $api->post('invites_information', 'App\Http\Controllers\Api\GroupActionsController@invites_information');
        $api->post('logout', 'App\Http\Controllers\Api\AuthController@logout');

        $api->post('profile', 'App\Http\Controllers\Api\ProfileController@get');
        $api->post('save_profile', 'App\Http\Controllers\Api\ProfileController@save');
        $api->post('save_bank_info', 'App\Http\Controllers\Api\ProfileController@save_bank_info');
    });
});
