<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InviteUser extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'group_invitations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['email', 'invitation_type', 'status', 'group_id', 'sender_id'];
}
