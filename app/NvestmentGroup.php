<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NvestmentGroup extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'nvestment_groups';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['payment_id', 'nvestment_amount', 'status'];

    public function master()
    {
        return $this->hasOne('App\GroupArchive', 'group_id')
            ->where('user_role', 'master');
    }
}
