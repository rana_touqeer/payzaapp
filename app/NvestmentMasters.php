<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NvestmentMasters extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'group_masters';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['master_id', 'group_id', 'payment_id'];

    /**
     * Relation to users table
     *
     * @var array
     */

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    /**
     * Relation to users table
     *
     * @var array
     */

    public function group()
    {
        return $this->belongsTo('App\NvestmentGroup', 'group_id');
    }

    /**
     * Relation to payment table
     *
     * @var array
     */
    public function payment()
    {
        return $this->belongsTo('App\PaymentDetails', 'payment_id');
    }


}
