<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Davibennun\LaravelPushNotification\Facades\PushNotification;

class PushNotifications extends Model
{
    protected $table = 'push_notifications';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'message', 'counter_status'
    ];

    public static function sendToIos($data){
        if($data['device_token'] != 'failed'){
            $message = PushNotification::Message( $data['message']);
            $send = PushNotification::app(['environment' => 'prduction',
                'certificate' => app_path()."/pem/apns-pro.pem",
                'passPhrase'  => '',
                'service'     => 'apns'])
                ->to($data['device_token'])
                ->send($message);
        }

    }
}
