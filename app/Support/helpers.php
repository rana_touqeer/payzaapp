<?php

use App\Follower;
use App\RoomMember;
use App\Message;
use App\MessageNotifications;
class Helperclass
{
    /**
     * Helper class for helper functions
     *
     * @return \Illuminate\Http\Response
     */
    static function formaterrors($errors)
    {
        $message = '';
        $errorsobj = $errors->toArray();
        foreach($errorsobj as $errors){
            foreach($errors as $error){
                $message.=$error;
            }
        }
        return $message;
    }

    static function formaterrors_fe($errors)
    {
        $errorsobj = $errors->toArray();
        $message = '<ul>';
        foreach($errorsobj as $errors){

            foreach($errors as $error){
                $message.="<li>".$error."</li>";
            }
        }
        $message.="</ul>";
        return $message;
    }


    static function setMailer()
    {
        $options['ssl']['verify_peer'] = FALSE;
        $options['ssl']['verify_peer_name'] = FALSE;

        // Send email notification
        $transport = \Swift_SmtpTransport::newInstance(
            \Config::get('mail.host'),
            \Config::get('mail.port'),
            \Config::get('mail.encryption')
        )
            ->setUsername(\Config::get('mail.username'))
            ->setPassword(\Config::get('mail.password'))
            ->setStreamOptions($options);

        $mailer = \Swift_Mailer::newInstance($transport);
        Mail::setSwiftMailer($mailer);
    }

    static function follow_link($follow = null, $follower_id = null){
        if(!$follow){
            return '<a class="link-follow" href="javascript:void(0)" data-attr="'.$follower_id.'"><img src="/images/icon-1-follow.svg" /></a>';
        }else{
            if($follow && $follow->follow_status == 1){
                return '<a class="link-unfollow" href="javascript:void(0)" data-attr="'.$follower_id.'"><img src="/images/icon-2-unfollow.svg" /></a>';
            }else{
                return '<a class="link-unfollow" href="javascript:void(0)" data-attr="'.$follower_id.'"><img src="/images/requested-follow.png" title="Follow request sent, resend request." /></a>';
            }
        }
    }

    /**
     * Orientate an image, based on its exif rotation state
     *
     * @param  Intervention\Image\Image $image
     * @param  integer $orientation Image exif orientation
     * @return Intervention\Image\Image
     */

    static function orientate($image, $name)
    {
        if(file_exists($image)){
            $image = new Imagick($image);
        }else{
            return true;
        }

        switch ($image->getImageOrientation()) {
            case Imagick::ORIENTATION_TOPLEFT:
                break;
            case Imagick::ORIENTATION_TOPRIGHT:
                $image->flopImage();
                break;
            case Imagick::ORIENTATION_BOTTOMRIGHT:
                $image->rotateImage("#000", 180);
                $image->setImageOrientation(Imagick::ORIENTATION_TOPLEFT);
                unlink(public_path() . '/images/post_pic/'.$name);
                file_put_contents (public_path() . '/images/post_pic/'.$name, $image);
                break;
            case Imagick::ORIENTATION_BOTTOMLEFT:
                $image->flopImage();
                $image->rotateImage("#000", 180);
                $image->setImageOrientation(Imagick::ORIENTATION_TOPLEFT);
                unlink(public_path() . '/images/post_pic/'.$name);
                file_put_contents (public_path() . '/images/post_pic/'.$name, $image);
                break;
            case Imagick::ORIENTATION_LEFTTOP:
                $image->flopImage();
                $image->rotateImage("#000", -90);
                $image->setImageOrientation(Imagick::ORIENTATION_TOPLEFT);
                unlink(public_path() . '/images/post_pic/'.$name);
                file_put_contents (public_path() . '/images/post_pic/'.$name, $image);
                break;
            case Imagick::ORIENTATION_RIGHTTOP:
                $image->rotateImage("#000", 90);
                $image->setImageOrientation(Imagick::ORIENTATION_TOPLEFT);
                unlink(public_path() . '/images/post_pic/'.$name);
                file_put_contents (public_path() . '/images/post_pic/'.$name, $image);
                break;
            case Imagick::ORIENTATION_RIGHTBOTTOM:
                $image->flopImage();
                $image->rotateImage("#000", 90);
                $image->setImageOrientation(Imagick::ORIENTATION_TOPLEFT);
                unlink(public_path() . '/images/post_pic/'.$name);
                file_put_contents (public_path() . '/images/post_pic/'.$name, $image);
                break;
            case Imagick::ORIENTATION_LEFTBOTTOM:
                $image->rotateImage("#000", -90);
                $image->setImageOrientation(Imagick::ORIENTATION_TOPLEFT);
                unlink(public_path() . '/images/post_pic/'.$name);
                file_put_contents (public_path() . '/images/post_pic/'.$name, $image);
                break;
            default: // Invalid orientation
                break;
        }
    }

    static function getLastNDays($days, $format = 'd/m'){
        $m = date("m"); $de= date("d"); $y= date("Y");
        $dateArray = array();
        for($i=0; $i<=$days-1; $i++){
            $dateArray[] = date($format, mktime(0,0,0,$m,($de-$i),$y));
        }
        return array_reverse($dateArray);
    }

    static function getRoomIds(){
        $user_id = Auth()->user()->id;
        $rooms = RoomMember::where('member_id',$user_id)->get();
        $room_names = array();
        if(count($rooms) > 0){
            foreach($rooms as $room){
                if($room->room){
                    $room_names[] = $room->room->name;
                }
            }
            return json_encode($room_names);
        }else{
            return json_encode('');
        }
    }

    static function get_unread_msgs_count(){
        $user_id = Auth::user()->id;
        return count(MessageNotifications::where('user_id', $user_id)->where('counter_status', 0)->get());
    }

    static function ajax_unread_msgs_count(){
        $user_id = Auth::user()->id;
        $unread_messages_count = count(MessageNotifications::where('user_id', $user_id)->where('counter_status', 0)->get());
        return response()->json(['status_code' => 200, 'unread_count' => $unread_messages_count, 'message' => 'success']);
    }

    static function decode_string_for_special_charecters($str){
        $text = str_replace(array("\r\n", "\n"), "\\n", $str);
        $text = str_replace('"','\"',$text);
        $text = str_replace('\\\"','\"',$text);
        return $text;
    }
}