<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function saveDeviceToken($user, $deviceToken = null){
        //remove token if attached to any other user
        //User::where('device_token',$deviceToken)->update(['device_token' => '']);

        //assign token to requested user
        $user_data = $user->toArray();
        $user = User::findorfail($user_data['id']);
        $user->device_token = $deviceToken;
        if($user->save()){
            return true;
        }
    }

    public function bank_info()
    {
        return $this->hasOne('App\UserBankInfo', 'user_id');
    }
}
