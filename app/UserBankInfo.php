<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserBankInfo extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_bank_info';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'bank_holder_name', 'swift_bic_code', 'account_number', 'bank_name', 'bank_address'];

    /**
     * Relation to users table
     *
     * @var array
     */

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
