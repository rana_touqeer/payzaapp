<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_masters', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('master_id')->unsigned();
            $table->integer('group_id')->unsigned();
            $table->integer('payment_id')->unsigned();
            $table->timestamps();
            $table->index('master_id');
            $table->foreign('master_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->index('group_id');
            $table->foreign('group_id')->references('id')->on('nvestment_groups')->onUpdate('cascade')->onDelete('cascade');
            $table->index('payment_id');
            $table->foreign('payment_id')->references('id')->on('payment_details')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_masters');
    }
}
