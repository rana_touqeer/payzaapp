<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_partners', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('investor_id')->unsigned();
            $table->integer('partners_id')->unsigned();
            $table->integer('group_id')->unsigned();
            $table->integer('payment_id')->unsigned();
            $table->string('position');
            $table->timestamps();

            $table->index('investor_id');
            $table->index('partners_id');
            $table->index('group_id');
            $table->index('payment_id');

            $table->foreign('investor_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('partners_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('group_id')->references('id')->on('nvestment_groups')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('payment_id')->references('id')->on('payment_details')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_partners');
    }
}
