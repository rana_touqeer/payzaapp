$(".social-media").click(function () {
    $(this).next(".social-nav").slideToggle();
});
$(".social-nav ul li").click(function () {
    var updateVal = $(this).html();
    $(".social-media span").html(updateVal);
    $(this).parent().parent().slideUp();
});
$(".menu-button").click(function () {
    $(".header-nav").slideToggle();
});
$(".link-search").click(function () {
    $(this).toggleClass("active");
    $("#search-drop-panel").slideToggle();
    $("#notifications-drop-panel").hide()
    $('.notifications-list').removeClass('active');

});
$(function () {
    $('[data-toggle="tooltip"]').tooltip()
});
$(function () {
    $('[data-toggle="popover"]').popover()
});

$(document).mouseup(function (e) {
    var container = $("#search-drop-panel, .link-search, #notifications-drop-panel, .notifications-list");

    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    {
        $('.link-search, .notifications-list').removeClass("active");
        $("#search-drop-panel").hide();
        $("#notifications-drop-panel").hide();
    }
});
//// stats chart
$(function () {
    $('#stats').highcharts({
        chart: {
            type: 'area'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: ['Aug 20', 'Aug 25', 'Aug 30', 'Sep 5', 'Sep 10', 'Sep 15', 'Sep 20'],
            tickmarkPlacement: 'on',
            title: {
                enabled: false
            }
        },
        yAxis: {
            title: {
                text: ''
            },
            labels: {
                formatter: function () {
                    return this.value / 1000;
                }
            }
        },
        tooltip: {
            split: true,
            valueSuffix: ' millions'
        },
        plotOptions: {
            area: {
                stacking: 'normal',
                lineColor: '#666666',
                lineWidth: 1,
                marker: {
                    lineWidth: 1,
                    lineColor: '#666666'
                }
            }
        },
        series: [{
            name: 'Users',
            data: [502, 635, 809, 947, 1402, 3634, 5268]
        }, {
            name: 'Posts',
            data: [106, 107, 111, 133, 221, 767, 1766]
        }, {
            name: 'Comments',
            data: [163, 203, 276, 408, 547, 729, 628]
        }]
    });
});

//confirmation box
function confirmDialog(message, params, onConfirm) {
    var fClose = function () {    //close
        modal.modal("hide");
    };

    var modal = $("#confirmModal");
    modal.modal("show");
    $("#confirmMessage").empty().append(message);
    if (params) {
        $("#confirmOk").empty().append(params.ok);
        $("#confirmCancel").empty().append(params.cancle);
    }
    $("#confirmOk").one('click', onConfirm);
    $("#confirmOk").one('click', fClose);
    $("#confirmCancel").one("click", fClose);
}


//auto-complete on top search section
$(function () {
    $("#search_nav").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/search-autocomplete",
                dataType: "json",
                type: "post",
                data: {
                    term: request.term
                },
                success: function (data) {
                    //response( data );                                       
                    response($.map(data, function (el) {
                        return {
                            //label: el.name
                            value: el.name
                        };
                    }));
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            //log( "Selected: " + ui.item.value + " aka " + ui.item.id );
            $('#search_nav').val(ui.item.value);
            $('#search_nav').trigger("enterKey");
        }
    });
});
function createCookie(name, value, days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}

Date.prototype.addHours = function (h) {
    this.setHours(this.getHours() + h);
    return this;
}

var tz = -(new Date().getTimezoneOffset() / 60);
var date = new Date();
date.setTime(date.getTime() + (7 * 24 * 60 * 60 * 1000));
expires = "; expires=" + date.toUTCString();
document.cookie = 'currentTimeZoneHoursDifference' + "=" + tz + expires + "; path=/";

function isImage(file) {
    var fileType = file["type"];
    var ValidImageTypes = ["image/gif", "image/jpeg", "image/png", "image/jpg"];
    if ($.inArray(fileType, ValidImageTypes) < 0) {
        return false;
    } else {
        return true;
    }
}

function isVideo(file) {
    var fileType = file["type"];
    var ValidVideoTypes = ["video/mpeg", "video/flv", "video/x-flv", "video/mp4", "video/x-mpegURL", "video/MP2T", "video/3gpp", "video/quicktime", "video/x-msvideo", "video/x-ms-wmv", "video/x-ms-wmv"];
    if ($.inArray(fileType, ValidVideoTypes) < 0) {
        return false;
    } else {
        return true;
    }
}

/**
 * Delete a post
 */
$(document).on('click', '.delete-post', function () {
    var post_id = $(this).attr("data-attr");
    var post_page = $(this).attr("data-target");
    var buttons = {'ok': 'Delete', 'cancle': 'Cancel'};
    confirmDialog('Are you sure to delete this post?', buttons, function () {
        //code to delete
        $.ajaxSetup({
            header: $('meta[name="_token"]').attr('content')
        });
        $.ajax({
            type: "POST",
            url: '/home/post/delete',
            //data: { user_id : $(this).attr("data-attr") },
            data: {post_id: post_id},
            success: function (data) {
                if (data.message == 'success') {
                    if(post_page == 'post_detail'){
                        window.location.href = '/home/newsfeed';
                    }else{
                        location.reload();
                    }

                } else {
                    console.log('not deleted');
                    var error = data.message;
                    $('#flash-'+post_id).addClass('alert alert-danger');
                    $('#flash-'+post_id).html(error);
                }
            }
        });
    });
});