/**
 * Created by touqeermuhammad on 4/25/17.
 */
$('textarea.mention').mentionsInput({
    onDataRequest:function (mode, query, callback) {
        $.getJSON('/post/get/followers', function(responseData) {
            responseData = _.filter(responseData, function(item) { return item.name.toLowerCase().indexOf(query.toLowerCase()) > -1 });
            callback.call(this, responseData);
        });
    }
});
