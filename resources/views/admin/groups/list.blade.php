@extends('layouts.admin.dashboard')
@section('title')
    : Manage Groups
@endsection

@section('styles')
    <link href="/css/jquery.popdown.css" rel="stylesheet" type="text/css">
@endsection

@section('content')
    <div class="center-container">
        <!--- Manage Users--->
        <div class="white-box users-holder">
            <h2>
                Manage Completed Groups
            </h2>
            <table class="manage-table" id="users-table">
                <thead>
                <tr>
                    <th class="small">Group ID</th>
                    <th class="small">Name</th>
                    <th class="small">Email</th>
                    <th class="small">Amount</th>
                    <th class="small">Status</th>
                    <th class="small">Created At</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="/js/jquery.popdown.js"></script>
    <script>
        $(document).ready(function(){
            var delete_url = '<?php echo url('admin/user/delete/') ?>';
            var block_url = '<?php echo url('admin/user/block/') ?>';
            var activate_url = '<?php echo url('admin/user/activate/') ?>';
            var profile_url = '<?php echo url('admin/user/profile/') ?>';
            var edit_url = '<?php echo url('admin/user/edit_profile/') ?>';
            $(function() {
                $('#users-table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: '{!! route('manage_completed_groups') !!}',
                    aaSorting: [[0, 'desc']],
                    "Sortable": true,
                    "sDom": '<"top">lrt<"bottom">ip',
                    columns: [
                        { data: 'id', name: 'id', "searchable": true},
                        {
                            data: 'name', name: 'name', 'render': function(data, type, row, meta){
                            var name_link = '<strong class="title-text">Name</strong>' +
                                    '<div class="full-length">' +
                                    '<a href="/admin/user/profile/'+ row.master.user_id + '" class="popdown">' +
                                    row.name +
                                    '</a>' +
                                    '</div>';
                            return name_link;
                        },
                        },
                        { data: 'email', name: 'email', 'render': function(data, type, row, meta){
                            var column_data = '<strong class="title-text">Email</strong>' +
                                    '<div class="full-length">' +
                                    row.email +
                                    '</div>';
                            return column_data;
                        },
                        },
                        { data: 'amount', name: 'amount', 'render': function(data, type, row, meta){
                            var column_data = '<strong class="title-text">Amount</strong>' +
                                    '<div class="full-length">' +
                                    row.amount +
                                    '</div>';
                            return column_data;
                        },
                        },
                        { data: 'status', name: 'status', 'render': function(data, type, row, meta){
                            var column_data = '<strong class="title-text">Status</strong>' +
                                    '<div class="full-length">' +
                                    row.status +
                                    '</div>';
                            return column_data;
                        },
                        },
                        { data: 'created_at', name: 'created_at', 'render': function(data, type, row, meta){
                            var column_data = '<strong class="title-text">Created At</strong>' +
                                    '<div class="full-length">' +
                                    row.created_at +
                                    '</div>';
                            return column_data;
                        },
                        },
                    ],
                    fnDrawCallback: function () {
                        $('.popdown').popdown({width:500});
                    }
                });
            });
        });
    </script>
@endsection