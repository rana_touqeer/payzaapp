

<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <!-- /.panel-heading -->
            <div class="panel-body">    

                <table class="manage-table">
                        <tr>
                            <td class="title">Username</td> <td>{{ $user->name }}</td>
                        </tr>
                    <tr>
                        <td class="title">Bank Account holder Name</td>
                        <td>{{ ($user->bank_info && $user->bank_info->bank_holder_name)?$user->bank_info->bank_holder_name:'N/A' }}</td>
                    </tr>
                    <tr>
                        <td class="title">Swift/Bic Code</td>
                        <td>{{ ($user->bank_info && $user->bank_info->swift_bic_code)?$user->bank_info->swift_bic_code:'N/A' }}</td>
                    </tr>
                    <tr>
                        <td class="title">Account Number</td>
                        <td>{{ ($user->bank_info && $user->bank_info->account_number)?$user->bank_info->account_number:'N/A' }}</td>
                    </tr>
                    <tr>
                        <td class="title">Bank Name</td>
                        <td>{{ ($user->bank_info && $user->bank_info->bank_name)?$user->bank_info->bank_name:'N/A' }}</td>
                    </tr>
                    <tr>
                        <td class="title">Bank Address</td>
                        <td>{{ ($user->bank_info && $user->bank_info->bank_address)?$user->bank_info->bank_address:'N/A' }}</td>
                    </tr>
                </table>

                
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
