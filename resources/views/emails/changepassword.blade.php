@extends('layouts.emails')

@section('title')
	<p style="color: white; font-size: 24px; font-weight: bold; margin:0; line-height: 100%">Update</p>
	<p style="font-size: 24px; font-weight: normal; color: white; margin:0; line-height: 100%;">your password</p>
@endsection

@section('content')
	<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td bgcolor="white" style="padding: 30px 35px">
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tbody>
					<tr>
						<td>
							<h1 style="color: #1977be; margin: 0; margin-bottom: 10px; padding: 0; font-size: 18px;">Dear {{ $data['name'] }}</h1>
							<table class="spacer">
								<tbody>
								<tr>
									<td height="25px" style="font-size:16px;line-height:16px;">&#xA0;</td>
								</tr>
								</tbody>
							</table>
							<p>Visit following URL to reset your password.</p>
							<p><a href="{{ URL::to('/') }}/newpassword/{{ $data['token'] }}" style="text-decoration: none;">{{ URL::to('/') }}/newpassword/{{ $data['token'] }}</a></p>
							<table class="spacer" cellpadding="0" cellspacing="0" border="0" width="100%">
								<tr>
									<td height="25px" style="font-size:25px; line-height:25px;">&#xA0;</td>
								</tr>
							</table>
							<p style="color: #1977be; margin: 0;">The NVST Team <br><a href="#" style="font-weight: bold; text-decoration: none;">admin@nvst.com</a></p>
						</td>
					</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</table>
@endsection