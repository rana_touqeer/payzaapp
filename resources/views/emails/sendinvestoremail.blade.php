@extends('layouts.emails')

@section('title')
	<p style="color: white; font-size: 24px; font-weight: bold; margin:0; line-height: 100%">Nvestment</p>
	<p style="font-size: 24px; font-weight: normal; color: white; margin:0; line-height: 100%;">invitation</p>
@endsection

@section('content')
	<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td bgcolor="white" style="padding: 30px 35px">
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tbody>
					<tr>
						<td>
							<h1 style="color: #1977be; margin: 0; margin-bottom: 10px; padding: 0; font-size: 18px;">Greetings!</h1>
							<table class="spacer">
								<tbody>
								<tr>
									<td height="25px" style="font-size:16px;line-height:16px;">&#xA0;</td>
								</tr>
								</tbody>
							</table>
							<p>{{$data['sender_name']}} just sent you an invitation to play at NvstApp as an investor partner. <br />
							Please install the <a href="javascript:void(0)">NvstApp</a> and start playing.</p>
							<table class="spacer" cellpadding="0" cellspacing="0" border="0" width="100%">
								<tr>
									<td height="25px" style="font-size:25px; line-height:25px;">&#xA0;</td>
								</tr>
							</table>
                            <p style="color: #1977be; margin: 0;">
                                Happy Nvesting! <br />
                                The NVST Team <br><a href="#" style="font-weight: bold; text-decoration: none;">admin@nvst.com</a>
                            </p>
						</td>
					</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</table>
@endsection