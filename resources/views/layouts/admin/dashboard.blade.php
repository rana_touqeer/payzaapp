<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Nvstapp @yield('title')</title>

    <!-- Bootstrap -->
    <link href="/css/vender/bootstrap.min.css" rel="stylesheet">
    <link href="/css/vender/bootstrap-custom-form.css" rel="stylesheet">
    <link href="/css/vender/font-awesome.css" rel="stylesheet">
    <link href="/css/vender/jquery.mCustomScrollbar.css" rel="stylesheet">
    <link href="/css/main.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <link href="/css/admin-style.css" rel="stylesheet">
    <link href="/css/custom.css" rel="stylesheet">
    @yield('styles')
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php
    use Illuminate\Support\Facades\Route;
    $currentPath= Route::getFacadeRoot()->current()->uri();
?>
<!--Header-->
<header id="header">
    <div class="center-container">
        <div class="header-logo">
            <a href="/admin/dashboard"><img src="/images/logo-screen.png" title="Admin Dashboard" alt="logo-png"/></a>
        </div>
        <div class="header-right">
            {{--<a class="link-search" href="#"><img src="/images/icon-nav-search.svg" alt="search" /></a>--}}
            @if(Auth::check())
                <a href="/admin/auth/logout" title="logout"><img src="/images/icon-nav-logout.svg" alt="logout" /></a>
            @endif
            <a href="#" class="visible-xs menu-button"><img src="/images/icon-nav-menu-lines.svg" alt="menu icon" /></a>
        </div>
        <nav class="header-nav admin">
            <ul>
                <li @if ($currentPath == 'admin/dashboard') class="active" @endif>
                    <a href="/admin/dashboard">
                        Manage Completed Groups
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</header>
<!--main wrapper-->
<section id="main-wrapper" class="main-section">
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if(Session::has('flash_message'))
        <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
    @endif
    @if (isset($errormessage))
        <div class="alert alert-danger" role="alert">{{ $errormessage  }}</div>
    @else
        @yield('content')
    @endif
</section>
<footer id="footer">
    <div class="center-container">
        <p class="login-copyrights">
            Copyright &copy; {{ date('Y') }} Nvstapp. All Rights Reserved
        </p>
    </div>
</footer>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="/js/vender/jquery.min.js"></script>
<script src="/js/vender/highcharts.js"></script>
<script src="/js/vender/exporting.js"></script>


<!--autocomplete-->
<link href="/css/autocomplete-styles.css" rel="stylesheet">
<script src="/js/vender/jquery-ui.js"></script>
<link href="/css/vender/jquery-ui.css" rel="stylesheet">
<script src="/js/custom.js"></script>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/js/vender/bootstrap.min.js"></script>
<script src="/js/vender/bootstrap-custom-form.js"></script>
<script src="/js/vender/jquery.mCustomScrollbar.min.js"></script>

{{--<script src="/js/custom.js"></script>--}}
@yield('scripts')
</body>
</html>