<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Admin</title>

        <!-- Bootstrap Core CSS -->
        <link href="/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

        <!-- Timeline CSS -->
        <link href="/bower_components/startbootstrap-sb-admin-2/dist/css/timeline.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="/bower_components/startbootstrap-sb-admin-2/dist/css/sb-admin-2.css" rel="stylesheet">


        <!-- Custom Fonts -->
        <link href="/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- Admin Custom CSS -->
        <link href="/css/admin-style.css" rel="stylesheet">

        @yield('styles')

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>

        <div id="wrapper">

            <!-- Navigation -->
            <nav style="margin-bottom: 0" role="navigation" class="navbar navbar-default navbar-static-top">
                <div class="navbar-header">
                    <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="#" class="navbar-brand">Biopage</a>
                </div>
                <!-- /.navbar-header -->

                <ul class="nav navbar-top-links navbar-right">



                    <!-- /.dropdown -->
                    <li class="dropdown">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle" aria-expanded="false">
                            <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="<?php echo url('admin/auth/logout') ?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                            </li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    <!-- /.dropdown -->
                </ul>
                <!-- /.navbar-top-links -->

                <div role="navigation" class="navbar-default sidebar">
                    <div class="sidebar-nav navbar-collapse">
                        <ul id="side-menu" class="nav in">

                            <li>
                                <a href="<?php echo url('admin/dashboard') ?>" class="<?php echo Request::path() == 'admin/dashboard' ? 'active' : ''; ?>"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                            </li>

                            <li>
                                <a href="<?php echo url('admin/user') ?>" class="<?php echo Request::path() == 'admin/user' ? 'active' : ''; ?>"><i class="fa fa-table fa-fw"></i> Manage Users</a>
                            </li>
                            <li>
                                <a href="<?php echo url('admin/post') ?>" class="<?php echo Request::path() == 'admin/post' ? 'active' : ''; ?>"><i class="fa fa-edit fa-fw"></i> Reported Posts</a>
                            </li>
                            <li>
                                <a href="<?php echo url('admin/email') ?>" class="<?php echo Request::path() == 'admin/email' ? 'active' : ''; ?>"><i class="fa fa-envelope fa-fw"></i> Manage Emails</a>
                            </li>
                            <li>
                                <a href="<?php echo url('admin/settings') ?>" class="<?php echo Request::path() == 'admin/settings' ? 'active' : ''; ?>"><i class="fa fa-cog fa-fw"></i> Settings</a>
                            </li>



                        </ul>
                    </div>
                    <!-- /.sidebar-collapse -->
                </div>
                <!-- /.navbar-static-side -->
            </nav>

            <div id="page-wrapper" style="min-height: 380px;">
                @if(Session::has('flash_message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('flash_message') }}</p>
                @endif 
                @include('errors.list')
                @yield('content')

                <!-- /.row -->
            </div>
            <!-- /#page-wrapper -->

        </div>

        <!-- jQuery -->
        <script src="/bower_components/jquery/dist/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="/bower_components/metisMenu/dist/metisMenu.min.js"></script>
        
        <!-- admin js -->
        <script src="/js/scripts.js"></script>
        
        
        @yield('scripts')
    </body>

</html>
