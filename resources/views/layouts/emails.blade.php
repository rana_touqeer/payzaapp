<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <title>Title</title>
</head>
<body>
<style>
    html{margin: 0; padding: 0;}
    body{
        box-sizing: border-box; width: 100% !important;
        -webkit-text-size-adjust: 100%;
        -ms-text-size-adjust: 100%;
        color: #4e4e4e;
        font-family: Calibri;
        font-weight: normal;
        padding: 0;
        margin: 0;
        Margin: 0;
        font-size: 15px;
        text-align: left;
        line-height: 1;
    }
    a{color: #1977be; text-decoration: none;}
    .container{width: 600px;}
    @media screen and (max-width: 650px){
        .container{width: 100%;}
    }
</style>
<table cellpadding="0" cellspacing="0" border="0" align="center" width="100%";>
    <tr>
        <td bgcolor="#efefef">
            <table cellpadding="0" cellspacing="0" border="0" align="center">
                <tr>
                    <td valign="top" class="container">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td style="padding: 22px 35px;" bgcolor="#1977be">
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                        <tr>
                                            <td valign="middle" width="80%">
                                                @yield('title')
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        @yield('content')
                        <table class="spacer" cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tr>
                                <td height="10px" style="font-size:10px; line-height:10px;">&#xA0;</td>
                            </tr>
                        </table>
                        <table align="center">
                            <tr>
                                <td>

                                    <table class="spacer" cellpadding="0" cellspacing="0" border="0" width="100%">
                                        <tr>
                                            <td height="17px" style="font-size:17px;line-height:17px;">&#xA0;</td>
                                        </tr>
                                    </table>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table class="spacer" cellpadding="0" cellspacing="0" border="0" width="100%">
                                        <tr>
                                            <td height="50px" style="font-size:50px;line-height:50px;">&#xA0;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>

                            </tr>
                        </table>
                        <table class="spacer">
                            <tbody>
                            <tr>
                                <td height="15px" style="font-size:16px;line-height:16px;">&#xA0;</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>